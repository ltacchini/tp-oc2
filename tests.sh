# LINUX
nasm -f elf enmascarar.asm
gcc -m32 -o enmascarar enmascarar.o enmascarar.c

## PRIMERA PRUEBA
# Preparo las imagenes.
gm convert images/p1_mascara.png images/p1_mascara.rgb
gm convert images/p1_img1.png images/p1_img1.rgb
gm convert images/p1_img2.png images/p1_img2.rgb

a=0 
# -lt is less than operator 
  
echo "INSTANCIA 1 ---------------------------------"
while [ $a -lt 100 ] 
do 
    ./enmascarar "images/p1_img2.rgb" "images/p1_img1.rgb" "images/p1_mascara.rgb" 128 128
      
    # increment the value 
    a=`expr $a + 1` 
done 

gm convert images/p2_1.png images/p2_1.rgb
gm convert images/p2_2.jpg images/p2_2.rgb
gm convert images/p2_mask.jpg images/p2_mask.rgb
a=0 
# -lt is less than operator 
echo "INSTANCIA 2 ---------------------------------"
while [ $a -lt 100 ] 
do 
    ./enmascarar "images/p2_1.rgb" "images/p2_2.rgb" "images/p2_mask.rgb" 256 256
      
    # increment the value 
    a=`expr $a + 1` 
done 

gm convert images/p3_1.jpg images/p3_1.rgb
gm convert images/p3_2.jpg images/p3_2.rgb
gm convert images/p3_mask.jpg images/p3_mask.rgb
a=0 
# -lt is less than operator 
echo "INSTANCIA 3 ---------------------------------"
while [ $a -lt 100 ] 
do 
    ./enmascarar "images/p3_1.rgb" "images/p3_2.rgb" "images/p3_mask.rgb" 500 500
      
    # increment the value 
    a=`expr $a + 1` 
done 


gm convert images/mask.jpg images/mask.rgb
gm convert images/img1.jpg images/img1.rgb
gm convert images/img2.jpg images/img2.rgb
a=0 
# -lt is less than operator 
echo "INSTANCIA 4 ---------------------------------"
while [ $a -lt 100 ] 
do 
    ./enmascarar "images/img2.rgb" "images/img1.rgb" "images/mask.rgb" 1920 1080
      
    # increment the value 
    a=`expr $a + 1` 
done 






