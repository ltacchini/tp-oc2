section .text
    ;global _enmascarar_asm ;windows
    global enmascarar_asm ;linux

;_enmascarar_asm: ;windows
enmascarar_asm: ;linux

    enter 0,0

    mov eax, [ebp+8]
    mov ecx, [ebp+12]
    mov edx, [ebp+16]

    mov esi, 0
    mov edi, 0
    
ciclo:
    movd xmm0, [eax+esi]    
    movd xmm1, [ecx+esi]
    cmp [edx+esi], edi
    jnz img1
    movaps xmm2,xmm1
    jmp result

img1:
    movaps xmm2,xmm0
    
result:         
    
    movd [eax+esi], xmm2
    add esi, 4 
    cmp esi, [ebp+20]
    jb ciclo

    leave
ret
