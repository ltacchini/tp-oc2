#include <stdio.h>
#include <stdlib.h>
#include<time.h>   

void read_file(unsigned char * fileName, unsigned char * buffer, int width, int height) {
    FILE *file;
    
    file = fopen((char *)fileName, "rb");
    if (!file)
    {
        fprintf(stderr, "Unable to open file %s", "images/mascara1.rgb");
    }    

    fread(buffer,width*height*3,sizeof(unsigned char),file);
    fclose(file);
}

void fill_array(int * array, unsigned char * buffer, int width, int height) {
    int i=0;

    while (i < width*height*3){
        
        if (buffer[i] > 0) {
            array[i/3] = 1;
        } else {
            array[i/3] = 0;
        }
        
        i = i +3;            
    }

}

extern void enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mask, int cant, unsigned char *result);

void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int width, int height) {
    
    unsigned char *buffer;
    unsigned char *buffer2;
    unsigned char *bufferMask;
    unsigned long fileLen;
    int i = 0;
    int arrayMask[width*height];
    fileLen = width*height*3;

    buffer=(unsigned char *)malloc(fileLen);
    buffer2=(unsigned char *)malloc(fileLen);
    bufferMask=(unsigned char *)malloc(fileLen);
    if (!buffer)
    {
        fprintf(stderr, "Memory error!");        
    }
    if (!buffer2)
    {
        fprintf(stderr, "Memory error!");     
    }
    //read_file(mask, buffer, width, height);    

    read_file(a, buffer, width, height);
    read_file(b, buffer2, width, height);
    read_file(mask, bufferMask, width, height);
    fill_array(arrayMask, bufferMask, width, height);
    FILE* file_c, * file_asm;
    file_c = fopen("images/salida_c.rgb", "wb"); //write the file in binary mode
    clock_t t1, t2;  

    printf("enmascarar.c ");
    t1 = clock();
    while (i < (fileLen)){
        
        if (arrayMask[i/3] == 0) {
            buffer[i] = buffer2[i];
            buffer[i+1] = buffer2[i+1];
            buffer[i+2] = buffer2[i+2];
        } 
        
        i = i + 3;                
    }
    t2 = clock();
    printf("%f",((float)(t2 - t1) / 1000000.0F));
    printf(" ");
    fwrite(buffer, fileLen, 1, file_c);
    fclose(file_c);

    
    unsigned char * result=(unsigned char *)malloc(fileLen);
    file_asm = fopen("images/salida_asm.rgb", "wb"); //write the file in binary mode

    t1 = clock();
    enmascarar_asm(buffer, buffer2, bufferMask, width*height*3, result);
    printf("enmascarar.asm ");
    t2 = clock();
    printf("%f",((float)(t2 - t1) / 1000000.0F )); 
    printf("\n");

    fwrite(buffer, fileLen, 1, file_asm);
    fclose(file_asm);

    free(buffer);
	free(buffer2);
    free(bufferMask);
}

int main(int argc, char* argv[]) {    
    int width, height;

    unsigned char * image1Name = (unsigned char *)argv[1];
    unsigned char * image2Name = (unsigned char *)argv[2];
    unsigned char * maskFile = (unsigned char *)argv[3];

    sscanf(argv[4], "%d", &width); 
    sscanf(argv[5], "%d", &height); 
    
    enmascarar_c(image1Name, image2Name, maskFile, width, height);  

   return 0;
}

