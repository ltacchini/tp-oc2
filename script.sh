# LINUX
nasm -f elf enmascarar.asm
gcc -m32 -o enmascarar enmascarar.o enmascarar.c

## PRIMERA PRUEBA
# Preparo las imagenes.
gm convert images/p1_mascara.png images/p1_mascara.rgb
gm convert images/p1_img1.png images/p1_img1.rgb
gm convert images/p1_img2.png images/p1_img2.rgb
# Ejecuto y guardo salidas.
./enmascarar "images/p1_img2.rgb" "images/p1_img1.rgb" "images/p1_mascara.rgb" 128 128
gm convert -size 128x128 -depth 8 images/salida_c.rgb images/p1_salida_c.png
gm convert -size 128x128 -depth 8 images/salida_asm.rgb images/p1_salida_asm.png

## SEGUNDA PRUEBA
# Preparo las imagenes.
gm convert images/p2_1.png images/p2_1.rgb
gm convert images/p2_2.jpg images/p2_2.rgb
gm convert images/p2_mask.jpg images/p2_mask.rgb
# Ejecuto y guardo salidas.
./enmascarar "images/p2_2.rgb" "images/p2_1.rgb" "images/p2_mask.rgb" 256 256
gm convert -size 256x256 -depth 8 images/salida_c.rgb images/p2_salida_c.png
gm convert -size 256x256 -depth 8 images/salida_asm.rgb images/p2_salida_asm.png


## TERCERA PRUEBA
# Preparo las imagenes.
gm convert images/p3_1.jpg images/p3_1.rgb
gm convert images/p3_2.jpg images/p3_2.rgb
gm convert images/p3_mask.jpg images/p3_mask.rgb
# Ejecuto y guardo salidas.
./enmascarar "images/p3_2.rgb" "images/p3_1.rgb" "images/p3_mask.rgb" 500 500
gm convert -size 500x500 -depth 8 images/salida_c.rgb images/p3_salida_c.png
gm convert -size 500x500 -depth 8 images/salida_asm.rgb images/p3_salida_asm.png

## CUARTA PRUEBA
# Preparo las imagenes.
gm convert images/mask.jpg images/mask.rgb
gm convert images/img1.jpg images/img1.rgb
gm convert images/img2.jpg images/img2.rgb

# Ejecuto y guardo salidas.
./enmascarar "images/img2.rgb" "images/img1.rgb" "images/mask.rgb" 1920 1080
gm convert -size 1920x1080 -depth 8 images/salida_c.rgb images/p4_salida_c.png
gm convert -size 1920x1080 -depth 8 images/salida_asm.rgb images/p4_salida_asm.png

exit
